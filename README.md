# tor-2e-compendium-fr

TOR2E - Compendium non officiel pour l'Anneau Unique 2nde édition - version française

## Compatibilité
Foundry VTT v11
            v12

## Installation

Installation depuis le Gestionnaire de Modules de FoundryVTT
- Rechercher TOR2E afin de trouver le module 
ou
- Installer le module via l'url du fichier Manifeste : https://gitlab.com/ghorin/tor-2e-compendium-fr/-/raw/main/module.json


## Description
Ce module contient un compendium contenant tous les objets utiles pour la création de personnages (PJs, PNJs, Adversaires) pour le système **tor2e** de l'Anneau Unique dans FoundryVTT (https://foundryvtt.com/packages/tor2e) :
- création des Traits Distinctifs, Vertus, Récompenses, Défauts d'Ombre, Compétences et Capacités Spéciales
- création des armes, armures et boucliers

Il permet également la création des Adversaires de.
- la 2nde edition
- la 1ère édition : 2 versions avec la conversion 1e => 2e par :
   - par CircleOfNom
   - par Ghorin

Tous ces objets et adversaires sont créés 
- avec leurs statistiques 
- sans texte ni image venant des livres officiels
- avec une référence vers le livre et la page contenant la description et l'éventuelle image associée

Toutes les images fournies dans ce compendium sont fournies
- soit par le système TOR2E => Traits Distinctifs, Vertus, Récompenses, Défauts, Capacités Spéciales, Compétences 
- ou par Foundry VTT (dans le dossier d'installation de Foundry VTT, dans le sous-dossier resources/app/public/icons) => armes, armures, boucliers 

Les Vertus Communes sont fournies en 2 versions lorsque cela est possible :
- une version normale
- une version avec Active Effect afin d'automatiser l'effet de la vertu

## License
L'Anneau Unique, La Terre du Milieu et Le Seigneur des Anneaux, ainsi que les personnages, objets, événements et lieux associés sont des marques déposées ou des marques enregistrées de Saul Zaentz Company d/b/a Middle-­earth Enterprises (SZC) et sont utilisés sous licence de Sophisticated Games Ltd. Tous droits réservés.

## Prêt à être utilisé / Prêt à être modifié
De par les droits de propriété et la licence, ce compendium ne contient aucun texte descriptif, aucun élément de règle, aucune image venant des livres de l'Anneau Unique. Mais tous les objets créés dans un monde d'un Gardien des Légendes sont alors dans un monde privé et le Gardien des Légendes peut alors modifier les objets pour y intégrer les textes descriptifs et images de sa copie personnelle des livres.


## Mode de fonctionnement
- Aller dans l'onglet "Compendium" de Foundry VTT
- Vous y trouverez les packs suivants :
   . tor-2e-compendium-fr : Equipement
   . tor-2e-compendium-fr : Caractéristiques
   . tor-2e-compendium-fr : Adversaires
- Vous pouvez soit 
   . ouvrir chaque pack et glisser/déposer vers l'onglet correspondant (Objets pour les Caractéristiques et l'équipement, Acteurs pour les Adversaires)
   . clic droit sur les packs et importer tout le contenu du pack vers votre monde  
Foundry VTT documentation about Compendiums :  https://foundryvtt.com/article/compendium/
